import './button.scss'
export default function ButtonPrimary(props) {
    let {isPrimary, isBig} = props
    let classlist = ['btn', 'px-4', 'text-white']
    if (isPrimary) classlist.push('btn-primary')
    if (isBig) classlist.push('isBig')
    return (
        <button className={classlist.join(' ')}>{props.children}</button>
    )
}