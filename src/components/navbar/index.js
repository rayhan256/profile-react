import { Link } from "react-router-dom"
import './navbar.scss'
import Logo from '../../assets/image/R.png'
import { Fade } from "react-reveal"
export default function Navbar() {
    return (
        <>
        <Fade top>
            <nav className="navbar navbar-expand-lg navbar-light">
                <div className="container">
                    <Link className="navbar-brand" to={'#'}><img src={Logo} width={50} height={50}/></Link>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                    <div className="navbar-nav">
                        <Link className="nav-link active" to={'#'}>Home</Link>
                        <Link className="nav-link" to={'#'}>Abouts</Link>
                        <Link className="nav-link" to={'#'}>Contact Us</Link>
                    </div>
                    </div>
                </div>
            </nav>
        </Fade>
    </>
    )
}