import {Link} from 'react-router-dom'
import ButtonPrimary from "../../components/button/buttonPrimary";
import Navbar from "../../components/navbar";
import './home.scss'
import HeaderImage from '../../assets/image/header.svg'
import { Fade } from 'react-reveal';
import AboutImage from '../../assets/image/minion.svg'
import ContactUs from '../../assets/image/contactus.svg'
export default function Home() {
    return (
        <>
            <Navbar />
           <Fade top delay="1000">
           <section className="container header">
                <div className="row">
                    <div className="col-6">
                        <div className="header-title text-primary">
                            Let`s Make Your Idea Up In The Air With Me
                        </div>
                        <div className="header-sub-content">
                            I`m Rayhan, web & android developer based on indonesia ready to help you out.
                        </div>
                        <ButtonPrimary isPrimary isBig>Know Me More</ButtonPrimary>
                    </div>
                    <div className="col-6">
                        <img src={HeaderImage} className="img-fluid" />
                    </div>
                </div>
            </section>
           </Fade>
           <Fade left delay="1000">
           <section className="container about">
                <div className="row">
                    <div className="col-3">
                        <Link className='click-me text-center' to={'#'}>Press Me !!</Link>
                        <br />
                        <img src={AboutImage} className='img-fluid' />
                    </div>
                    <div className="col-9">
                        <div className="col-12">
                            <div className="box">
                                Hi There, I’am Rayhan Rahmat Aziz Gamawanto. Graduated From LP3I Polytechnic and i will be your assistant to make your idea comes to reality. By the way, i’m a minion :D
                            </div>
                        </div>
                        <div className="col-12">
                            <div className="box reverse text-center d-flex justify-content-around align-items-center flex-column">
                                <span>Is there anything that i can help you out ? </span>
                                <ButtonPrimary isPrimary isBig className="my-4">Please Let Me Know</ButtonPrimary>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
           </Fade>
            <Fade right delat="1000">
            <section className="container contact-us">
                <div className="row">
                    <div className="col-6">
                        <div className="contact-us-title text-primary">
                            Let Minions Help Finish Your Jobs
                        </div>
                        <form action="#" method="post">
                            <div className="my-4">
                                <input type="text" placeholder='Email' className="form-control" />
                            </div>
                            <div className="my-4">
                                <input type="text" placeholder='Phone' className="form-control" />
                            </div>
                            <div className="my-4">
                                <textarea name="" id="" cols="30" rows="10" className="form-control">Describe Your Idea Here...</textarea>
                            </div>
                        </form>
                    </div>
                    <div className="col-6">
                        <img src={ContactUs} alt="" className='img-fluid' />
                    </div>
                </div>
            </section>
            </Fade>
        </>
    )
}